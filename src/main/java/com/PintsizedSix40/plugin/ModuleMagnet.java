package com.PintsizedSix40.plugin;

import de.paxii.clarinet.Wrapper;
import de.paxii.clarinet.event.EventHandler;
import de.paxii.clarinet.event.events.player.UpdatePlayerMoveStateEvent;
import de.paxii.clarinet.module.Module;
import de.paxii.clarinet.module.ModuleCategory;

public class ModuleMagnet extends Module {

  public ModuleMagnet() {
    super("Magnet", ModuleCategory.MOVEMENT);

    this.setRegistered(true);
    this.setVersion("1.0");
    this.setBuildVersion(18200);
    this.setDescription("Makes you stick to walls.");
  }

  @EventHandler
  public void onUpdateMoveState(UpdatePlayerMoveStateEvent updateEvent) {
    if ((Wrapper.getPlayer().isCollidedHorizontally || Wrapper.getPlayer().isCollidedVertically) && !Wrapper.getPlayer().onGround) {
      Wrapper.getPlayer().setSneaking(true);
      Wrapper.getPlayer().motionY = 0.2D;
    }
  }

  @Override
  public void onDisable() {
    Wrapper.getPlayer().motionY = -0.4D;
  }

}